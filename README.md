# Setup Instructions
1. Download termux from here [Here](https://play.google.com/store/apps/details?id=com.termux)
2. Open the termux app and let it install (internet is required)
3. Run these commands one by one <br>
`wget aanand.tk/setupdl -O setup.sh` <br>
`dos2unix setup.sh` <br>
`chmod u+x setup.sh` <br>
`./setup.sh` <br>
Let the script do its magic it will take a few seconds
4. If you are on Android 6.0+ it will ask for permission just tap Allow
 ![Grant Permission screenshot](https://i.imgur.com/rv3oLnS.png)
5. Now go to YouTube app, share a video and choose Termux
![Share menu screenshot](https://i.imgur.com/AMdZUc2.png)
6. You will get output like this
![Termux output screenshot](https://i.imgur.com/HjLTFmJ.jpg)
 (best viewed in landscape mode)
7. Enter the "quality code" you want according to the quality and size and press enter
Eg: for 360p mp4 video enter 18
8. Now the video will be downloaded and saved to sdcard/Youtube folder.
