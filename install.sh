#!/data/data/com.termux/files/usr/bin/bash
# 4/05/2017 Gabi Tiplea

echo -e "Requesting acces to storage\n"
termux-setup-storage
sleep 5

echo -e "Installing python and ffmpeg\n"
pkg install -y python ffmpeg

echo -e "Installing youtube-dl\n"
yes | pip install youtube-dl

echo -e "Creating the Youtube folder to download the files\n"
mkdir ~/storage/shared/Youtube

echo -e "Creating bin folder\n"
mkdir ~/bin

echo -e "Downloading and installing termux-url-opener\n"
wget  https://gitlab.com/shaileshaanand/youtubedl_in_termux/raw/master/termux-url-opener -O ~/bin/termux-url-opener
dos2unix ~/bin/termux-url-opener


echo -e "\n"
echo -e "Copyright 2017 Gabi Tiplea\n"
echo -e "Modified by Shailesh Aanand (https://gitlab.com/shaileshaanand)\n"
